﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using CommonLibrary.Services;
using ServerApp.Services;

namespace ServerApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                TcpServerChannel channel = new TcpServerChannel(9999);
                ChannelServices.RegisterChannel(channel, false);
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(TaxService), "ITaxService",WellKnownObjectMode.SingleCall);
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(SellService), "ISellService", WellKnownObjectMode.SingleCall);
                Console.WriteLine("The server is running...");
                Console.ReadKey();
            }
            catch
            {
                Console.WriteLine("Error - Cannot start server");
            }
        }
    }
}
