﻿using CommonLibrary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLibrary.Entities;

namespace ServerApp.Services
{
    public class TaxService : MarshalByRefObject, ITaxService
    {
        public int CalculateTax(Car car)
        {
            int sum = CalculateSumByEngineSize(car.EngineSize);

            Console.WriteLine("The tax for the car is: " + (car.EngineSize / 200) * sum);
            return (car.EngineSize/200)*sum;
        }

        private int CalculateSumByEngineSize(int engineSize)
        {
            if(engineSize<=1600)
            {
                return 8;
            }
            if(engineSize<=2000)
            {
                return 18;
            }
            if(engineSize<=2600)
            {
                return 72;
            }
            if(engineSize<=3000)
            {
                return 144;
            }
            return 290;
        }
    }
}
