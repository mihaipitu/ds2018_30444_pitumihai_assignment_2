﻿using CommonLibrary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLibrary.Entities;

namespace ServerApp.Services
{
    public class SellService : MarshalByRefObject, ISellService
    {
        public double CalculateSellingPrice(Car car)
        {
            double price = 0;
            if (2018 - car.Year < 7)
            {
                price = car.Price - (car.Price / 7) * (2018 - car.Year);
            }
            Console.WriteLine("The selling price for the car is: " + price);
            return price;
        }
    }
}
