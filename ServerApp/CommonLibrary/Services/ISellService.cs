﻿using CommonLibrary.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary.Services
{
    public interface ISellService
    {
        double CalculateSellingPrice(Car car);
    }
}
