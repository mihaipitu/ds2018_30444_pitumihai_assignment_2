﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary.Entities
{
    [Serializable]
    public class Car
    {
        public int Year { get; set; }
        public int EngineSize { get; set; }
        public double Price { get; set; }
    }
}
