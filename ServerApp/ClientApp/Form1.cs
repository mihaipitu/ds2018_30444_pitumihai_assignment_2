﻿using CommonLibrary.Entities;
using CommonLibrary.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    public partial class Form1 : Form
    {
        private ITaxService taxService;
        private ISellService sellService;

        public Form1(ITaxService TaxService,ISellService SellService)
        {
            InitializeComponent();
            taxService = TaxService;
            sellService = SellService;
        }

        private void taxButton_Click(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(yearTextBox.Text);
            int engineSize = Convert.ToInt32(engineSizeTextBox.Text);
            double price = Convert.ToDouble(priceTextBox.Text);

            Car c = new Car();
            c.Year = year;
            c.EngineSize = engineSize;
            c.Price = price;

            int tax = taxService.CalculateTax(c);

            taxLabel.Text = tax.ToString();
        }

        private void sellButton_Click(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(yearTextBox.Text);
            int engineSize = Convert.ToInt32(engineSizeTextBox.Text);
            double price = Convert.ToDouble(priceTextBox.Text);

            Car c = new Car();
            c.Year = year;
            c.EngineSize = engineSize;
            c.Price = price;

            double sellPrice = sellService.CalculateSellingPrice(c);

            sellPriceLabel.Text = sellPrice.ToString();
        }
    }
}
