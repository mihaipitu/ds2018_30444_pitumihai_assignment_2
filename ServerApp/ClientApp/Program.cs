﻿using CommonLibrary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            try
            {
                ChannelServices.RegisterChannel(new TcpClientChannel(), false);
                ITaxService taxService = (ITaxService)(Activator.GetObject(typeof(ITaxService), "tcp://localhost:9999/ITaxService"));
                ISellService sellService = (ISellService)(Activator.GetObject(typeof(ISellService), "tcp://localhost:9999/ISellService"));

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1(taxService,sellService));
            }
            catch
            {
                Console.WriteLine("Error - Can't connect to the server");
            }
        }
    }
}
